<?php
/**
 *	Web123 WordPress Theme
 *
 */

// This will enqueue style.css of child theme

function enqueue_childtheme_scripts() {
	wp_enqueue_style( 'web123-child', get_stylesheet_directory_uri() . '/style.css' );
	wp_enqueue_script( 'web123', esc_url( trailingslashit( get_stylesheet_directory_uri() ) . 'js/web123-child.min.js' ) );
}
add_action( 'wp_enqueue_scripts', 'enqueue_childtheme_scripts', 100 );



function custom_login_logo() {
    echo '<style type="text/css">
        h1 a { background-image:url(https://www.web123.com.au/wp-content/uploads/2016/09/logo-retina.png) !important; }
				.login h1 a { width: 100%!important; background-size: 160px!important; height: 200px!important;}
				body {background-color: #000!important; }
    </style>';
}

add_action('login_head', 'custom_login_logo');


/* Hide WP version strings from scripts and styles
 * @return {string} $src
 * @filter script_loader_src
 * @filter style_loader_src
 */
function remove_wp_version_strings( $src ) {
     global $wp_version;
     parse_str(parse_url($src, PHP_URL_QUERY), $query);
     if ( !empty($query['ver']) && $query['ver'] === $wp_version ) {
          $src = remove_query_arg('ver', $src);
     }
     return $src;
}
add_filter( 'script_loader_src', 'remove_wp_version_strings' );
add_filter( 'style_loader_src', 'remove_wp_version_strings' );

/* Hide WP version strings from generator meta tag */
function wpmudev_remove_version() {
return '';
}
add_filter('the_generator', 'wpmudev_remove_version');

add_action('login_head', 'custom_login_logo');

function wpbsearchform( $form ) {

    $form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
    <div class="cform2">
    <input type="text" value="' . get_search_query() . '" name="s" class="class-search-input"id="s" placeholder="Search" /><input type="image" id="searchsubmit"  src="/wp-content/uploads/2017/07/n_button_search.png" class="search-img-resp"  />
    </div>
    </form>';

    return $form;
}

add_shortcode('wpbsearch', 'wpbsearchform');


function wpbsearchformsidebar( $form ) {

    $form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
    <div class="cform2">
	<label style="color: #000;font-size: 21px;font-weight: 500;">Search</label><br>
	<div class="input-class">
    <input type="text" class="class-searchinput" value="' . get_search_query() . '" name="s" id="s" placeholder="keyword" /><br>
	</div>
	<div class="submit-class">
	<input type="submit" id="searchsubmit" class="class-searchsubmit" value="Search"   />
	</div>
    </div>
    </form>';

    return $form;
}

add_shortcode('wpbsidbarsearch', 'wpbsearchformsidebar');
